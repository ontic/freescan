package it.polito.dbdmg.ontic

import org.scalatest.FunSuite
import org.apache.log4j.{Logger,Level}
import org.apache.spark.{SparkConf,SparkContext}
import it.polito.dbdmg.ontic.io.{ParseSettings,IOHandler}
import org.alitouka.spark.dbscan.{DbscanSettings,Dbscan}
import it.polito.dbdmg.ontic.preprocessing.{DBScanPreprocessor,DBSCANPreprocessingSettings}
import org.alitouka.spark.dbscan.spatial.Point
import org.alitouka.spark.dbscan.util.io.IOHelper
import it.polito.dbdmg.ontic.io.RapidminerTextConverter
import it.polito.dbdmg.ontic.io.TextPoint
import org.apache.spark.rdd.RDD
import org.apache.spark.SparkContext._
import it.polito.dbdmg.ontic.preprocessing.DBSCANPreprocessingSettings.NormalizationTechnique
import org.alitouka.spark.dbscan.spatial.rdd.PointsPartitionedByRegionRDD
import org.alitouka.spark.dbscan.spatial.rdd.PartitioningSettings
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.linalg.distributed.RowMatrix



class SVD extends FunSuite {
 
  val conf=new SparkConf().setMaster("local[*]").setAppName("Test")//.set("DebugOutputPath", "/Users/mark9/debug/debug")
  val sc=new SparkContext(conf)
  
  test("Using SVD"){
    val logger = Logger.getLogger("it.polito.dbdmg.ontic.Logger")
    
    logger.setLevel(Level.INFO)
    Logger.getRootLogger.setLevel(Level.WARN)
    
    
    
    //reading data
    val parseSettings = new ParseSettings()
                             .withSeparator(";")
                             .withParser("it.polito.dbdmg.ontic.io.RapidminerTextConverter")
    
    //var data = IOHandler.readTextFile(sc, parseSettings,"/Users/mark9/Desktop/slide_dataset.txt")
    var data = IOHandler.readTextFile(sc, parseSettings,"/Users/mark9/crozza_full.csv")
                .filter(x => !x.coordinates.exists { x => x.isNaN() })
    //var data = IOHandler.readTextFile(sc, parseSettings,"hdfs://localhost:8020/user/mark9/polito/examples/test_points.txt")
    
    //svd transformation
    val svdMat = new RowMatrix(data.map(p=>Vectors.dense(p.coordinates.toArray[Double])))
    
    val svd=svdMat.computeSVD(50, computeU = true)
    data = svd.U.rows.map(r => new Point(r.toArray))
    
    
    val startingTime = System.currentTimeMillis()
    //preprocessing
    val dbscanPreprocessingSettings =  new DBSCANPreprocessingSettings()
                                        .withEpsilonPolicy("BeforeFirstDensityDecrease")
                                        .withEpsStep(0.001)                                       
    
    for(i <- 1 to 3){
      //preprocess
      val startingTime = System.currentTimeMillis()
      val preprocessor = new DBScanPreprocessor(sc, data, dbscanPreprocessingSettings)
                               
      val (minPts, eps) = preprocessor.preprocess()//single eps-minpts
      data=preprocessor.data

      val endPreprocessTime = System.currentTimeMillis()
      logger.info("Total time to preprocess data: "+(endPreprocessTime-startingTime)/1000+" s")
      
      //clustering
      val clusteringSettings = new DbscanSettings ()
        .withEpsilon(eps).withNumberOfPoints(minPts+1)

      logger.debug("I am using "+minPts+" as minPoints and "+eps+" as eps")
      val pSett = new PartitioningSettings()
                      //.withNumberOfSplitsWithinPartition(1)
                      
      
      val model = Dbscan.train (data, clusteringSettings, pSett)
      IOHandler.saveClusteringResult(model, "/Users/mark9/test_svd_50_new/"+i)
      logger.info("Total time to process: "+(System.currentTimeMillis()-endPreprocessTime)/1000+" s")
      
      //update data
      data=model.noisePoints.map{ p => new Point(p.coordinates) }
      logger.info("Count of noise points is "+data.count())
      
    }
  }
  
}