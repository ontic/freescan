package it.polito.dbdmg.ontic

import it.polito.dbdmg.ontic.util.ManualDBSCANCLParser
import org.apache.log4j.Logger
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import it.polito.dbdmg.ontic.io.ParseSettings
import it.polito.dbdmg.ontic.preprocessing.DBSCANPreprocessingSettings
import it.polito.dbdmg.ontic.io.IOHandler
import it.polito.dbdmg.ontic.preprocessing.DBScanPreprocessor
import org.alitouka.spark.dbscan.DbscanSettings
import org.alitouka.spark.dbscan.Dbscan
import org.alitouka.spark.dbscan.spatial.Point
import org.alitouka.spark.dbscan.DbscanModel

object ManualDBSCAN {
def main(args: Array[String]): Unit = {
    //parsing arguments
    val clArgs:ManualDBSCANCLParser = new ManualDBSCANCLParser("Ontic Spark Manual DBSCAN")
    if(clArgs.parse(args)){
  
      //setting log levels
      val logger = Logger.getLogger("it.polito.dbdmg.ontic.Logger")
      
      logger.setLevel(clArgs.args.onticLogLevel)
      Logger.getRootLogger.setLevel(clArgs.args.sparkLogLevel)
      
      //creating spark context
      var conf=new SparkConf().setMaster(clArgs.args.masterUrl)
                    .setAppName("Ontic Spark DBSCAN")
      if(clArgs.args.debugOutputPath.isDefined)              
         conf=conf.set("DebugOutputPath", clArgs.args.debugOutputPath.get)
         
      logger.debug("Spark master: " +clArgs.args.masterUrl)
         
      
      val sc=new SparkContext(conf)
      
      //set up parsing settings
      val parseSettings = new ParseSettings()
                               .withSeparator(clArgs.args.parserSeparator)
                               .withParser(clArgs.args.parserClassName)
      
      logger.debug("Parse settings:\r\n\tseparator : " +parseSettings.textFileParser.separator+"\r\n\tclass : " +parseSettings.textFileParser.getClass.getCanonicalName )
      
      //set up preprocessing settings
      val preprocessingSettings =  new DBSCANPreprocessingSettings()
                                          .withDistanceMeasureSuite(clArgs.args.distanceMeasureSuite.getClass.getCanonicalName)
                                          .withNormalizationTechnique(clArgs.args.normalizationTechnique.toString())
                                          //.withNumberOfPointsPerPartition(n)
      logger.debug("Distance measure suite : "+preprocessingSettings.distanceMeasureSuite.getClass.getCanonicalName)
      logger.debug("Normalization technique : "+preprocessingSettings.normalizationTechnique)
      
      var data = IOHandler.readTextFile(sc, parseSettings,clArgs.args.inputPath)
      
      if(preprocessingSettings.distanceMeasureSuite.isInstanceOf[org.alitouka.spark.dbscan.spatial.CosineAngleDistanceSuite]){
        data = data.filter { x => x.coordinates.exists { x => x!=0.0 } }
      }
      
      var preprocessor = new DBScanPreprocessor(sc, data, preprocessingSettings)
      
      data = preprocessor.normalize()
      data.persist()
      
      val startingTime = System.currentTimeMillis()
      val clusteringSettings = new DbscanSettings ()
        .withEpsilon(clArgs.args.epsilon).withNumberOfPoints(clArgs.args.minPoints)
        .withDistanceMeasureSuite(clArgs.args.distanceMeasureSuite)
      
      
      val model = Dbscan.train (data, clusteringSettings)
      //to add filtered data
      model.allPoints.union(preprocessor.filteredData.map(p => new Point(p.coordinates).withClusterId(DbscanModel.NoisePoint)))
      
      IOHandler.saveClusteringResult(model,clArgs.args.outputPath)
      logger.info("Total time to process: "+(System.currentTimeMillis()-startingTime)/1000+" s")
      
      
    }
  }
}