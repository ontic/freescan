package it.polito.dbdmg.ontic.io

import org.alitouka.spark.dbscan.spatial.Point

class RapidminerTextConverter extends IOParser {
  def parse(line: String): Point = {
    val els = line.split(separator)

    try{
      val coords = els.slice(0, els.length-2)
      val p = new TextPoint(coords.map(x=>x.toDouble))
      p.text=els(els.length-2)
      return p
    }catch {
      case e :NumberFormatException => { 
        e.printStackTrace()
        return null
       }
    }
     
    null
    
    
  }
}