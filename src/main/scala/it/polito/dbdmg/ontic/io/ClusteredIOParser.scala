package it.polito.dbdmg.ontic.io

import org.alitouka.spark.dbscan.spatial.Point

trait ClusteredIOParser extends Serializable {
  var separator:String = ","
  def parse(line:String):(Int, Array[Double])
}