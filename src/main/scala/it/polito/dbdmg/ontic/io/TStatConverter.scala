package it.polito.dbdmg.ontic.io

import org.alitouka.spark.dbscan.spatial.Point


/** @author mark9
*
*/
class TStatConverter extends IOParser with Serializable{

 override def parse(line:String):Point = {


   val els = line.split(separator)

   val index1 = Array( 73, 29 , 8, 52, 88 )




   try{
     val features1 = index1.map(i => els(i).toDouble)
     val scaledFeatures = Array(els(23).toLong << els(16).toInt, //scale RWIN by WIN_SCALE bits
       els(67).toLong << els(60).toInt,
       els(22).toLong << els(16).toInt,
       els(66).toLong << els(60).toInt).map(_.toDouble)

     val features = Array.concat(features1, scaledFeatures)

     val preorderingS : Double = els(81).toDouble / els(46).toDouble
     val pnetduplicateS : Double= els(82).toDouble / els(46).toDouble

     val derivateFeatures :  Array[Double] = Array(preorderingS, pnetduplicateS)
     //RTT_MIN_S; RTT_MIN_C; DATA_BYTES_C; DATA_BYTES_S; FLOW_DURATION; WIN_MIN_C; WIN_MIN_S; WIN_MAX_C; WIN_MAX_S; PREORDERING_S; PNET_DUPLICATE_S
     return new Point(Array.concat(features, derivateFeatures))
   }catch {
     case e :NumberFormatException => {
       e.printStackTrace()
       return null
      }
   }

   null

 }

}