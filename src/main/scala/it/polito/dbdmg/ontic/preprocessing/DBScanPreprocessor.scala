package it.polito.dbdmg.ontic.preprocessing

import org.apache.spark.rdd.RDD
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import scala.collection.mutable.ListBuffer
import org.apache.spark.rdd.MappedRDD
import org.apache.spark.Accumulator
import org.apache.spark.broadcast.Broadcast
import org.apache.log4j.Logger
import org.alitouka.spark.dbscan.spatial.Point
import it.polito.dbdmg.ontic.preprocessing.DBSCANPreprocessingSettings.EpsilonPolicies
import org.alitouka.spark.dbscan.spatial.rdd.PointsPartitionedByRegionRDD
import org.alitouka.spark.dbscan.spatial.rdd.PartitioningSettings
import org.alitouka.spark.dbscan.DbscanSettings
import scala.collection.mutable.HashMap
import scala.collection.SortedMap
import scala.collection.mutable.LinkedList
import org.apache.spark.rdd.ShuffledRDD
import it.polito.dbdmg.ontic.preprocessing.DBSCANPreprocessingSettings.MinPointsPolicies
import org.alitouka.spark.dbscan.spatial.CosineAngleDistanceSuite
import org.alitouka.spark.dbscan.util.math.HyperSphericalCoordinates
import org.alitouka.spark.dbscan.spatial.sector.HyperSector
import org.alitouka.spark.dbscan.spatial.BoundsInOneDimension
import it.polito.dbdmg.ontic.preprocessing.DBSCANPreprocessingSettings.NormalizationTechnique
import org.apache.zookeeper.KeeperException.UnimplementedException
import it.polito.dbdmg.ontic.statistics.ZDistribution


object DBScanPreprocessor{
  
  val logger = Logger.getLogger("it.polito.dbdmg.ontic.Logger")

}


class DBScanPreprocessor(sc:SparkContext, /*private[this]*/ var data:RDD[Point], settings:DBSCANPreprocessingSettings) {
  
 //private[DBScanPreprocessor] val broadcastedSetting:Broadcast[DBScanPreprocessingSettings]=sc.broadcast(settings) 
  var filteredData:RDD[Point]=sc.emptyRDD[Point]
  lazy val zDistribution=ZDistribution(data.map(p=>p.coordinates.array))
  //computing min and max
  lazy val minAndMax = data.aggregate(Array.fill(data.first().coordinates.length)(BigDecimal(Double.MaxValue), BigDecimal(Double.MinValue)))(
    (minsMaxs,current) => {
      minsMaxs.zip(current.coordinates.array).map( (x)=> (if(x._1._1<=x._2) x._1._1 else BigDecimal(x._2), if(x._1._2>=x._2) x._1._2 else BigDecimal(x._2) ) )
    },
    (minsMaxs1, minsMaxs2) => {
      minsMaxs1.zip(minsMaxs2).map({
        case (a, b) => ( if(a._1<b._1) a._1 else b._1, if(a._2>b._2) a._2 else b._2)
      })
    })


                            
  private def computeMinimumDensity(partitionedData:PointsPartitionedByRegionRDD):Double = {
    var d=0.0
   if(settings.distanceMeasureSuite.isInstanceOf[CosineAngleDistanceSuite]){
     d=partitionedData.count()
     val numberOfDimensions=partitionedData.first()._2.coordinates.length
     val minDefaultPoint = new HyperSphericalCoordinates (1.0,Array.fill (numberOfDimensions-1)(Math.PI))
     val maxDefaultPoint = new HyperSphericalCoordinates (1.0, Array.fill (numberOfDimensions-1)(0.0))
     minDefaultPoint.Phis(numberOfDimensions-2) = Math.PI*2.0
    
     val sphericalCoordsRDD = partitionedData.map(p => HyperSphericalCoordinates.fromCartesian(p._2.coordinates.toArray))
    
     val mins = sphericalCoordsRDD.fold(minDefaultPoint)( (min, p) => new HyperSphericalCoordinates(1.0,  min.Phis.zip(p.Phis).map(
        (x) => Math.min(x._1,x._2)    
     ).toArray ) )
    
     val maxs = sphericalCoordsRDD.fold(maxDefaultPoint)( (max, p) => new HyperSphericalCoordinates(1.0,  max.Phis.zip(p.Phis).map(
      (x) => Math.max(x._1,x._2)    
     ).toArray ) )
    
     mins.Phis.zip(maxs.Phis).foreach( b => {
       if(b._2!=b._1){
         d/=(b._2-b._1)
       }
     })
   }else{
     val broadcastedBoxes = sc.broadcast(partitionedData.boxes)
      
      val densityParams = partitionedData.mapPartitionsWithIndex((index, iter)=>{
        var volume = 1.0
        val size = iter.length
        if(size==0){
          Seq().iterator
        }else{
          broadcastedBoxes.value
            .find { x => x.partitionId == index }.get
            .bounds.foreach( x => { 
               //DBScanPreprocessor.logger.debug("Side length : "+x.length)
               volume=x.length*volume 
            })
          //DBScanPreprocessor.logger.debug("Volume : "+volume+" and "+size+" points")
          if(volume==0){
            Seq().iterator
          }else{
            Seq(size.toDouble/volume).iterator
          }
        }
      })
      .aggregate((0.0,0))((x,y) => (x._1+y, x._2+1), (x,y)=>(x._1+y._1,x._2+y._2))
      DBScanPreprocessor.logger.debug("Volumes : "+densityParams._1+" and "+densityParams._2+" counts")
      if(densityParams._2 == 0){
        d=0.0  
      }else{
        d=densityParams._1/densityParams._2.toDouble
      }
     }
    d
  }                          
                            
  /**
   * This method invokes the computeEpsilon and computeMinPoints methods, 
   * then return the values chosen.
   * 
   */
  def preprocess(): (Int, Double) = {
    
    val partitionedData = partitioning()
    
    val broadcastedSetting = sc.broadcast(settings)
    val density = computeMinimumDensity(partitionedData)
    
    DBScanPreprocessor.logger.info("Density calculated as "+density)
    
    val epsChosen = computeEpsilon(partitionedData, broadcastedSetting, density)
    
    DBScanPreprocessor.logger.info("Epsilon chosen is "+epsChosen)
    
    
    val minPoints = computeMinPoints(partitionedData, broadcastedSetting, epsChosen)
    
    //DBScanPreprocessor.logger.debug("not ceiled minpoints : "+(density*Math.pow(epsChosen, partitionedData.boundingBox.bounds.length)))
    
    //val minPoints:Int = (density*Math.pow(epsChosen, partitionedData.boundingBox.bounds.length)).ceil.toInt
    
    DBScanPreprocessor.logger.info("minPoints chosen is "+minPoints)
    (minPoints, epsChosen)
    
  }
/**
   * This method invokes the computeEpsilon and computeMinPoints methods, 
   * then return the values chosen.
   *
   */
  def preprocessMultiple(): Array[(Int, Double)] = {

    var result = new Array[(Int, Double)](settings.maxSuggestedEpsilon)

    val partitionedData = partitioning()

    val broadcastedSetting = sc.broadcast(settings)
    val density = computeMinimumDensity(partitionedData)

    DBScanPreprocessor.logger.info("Density calculated as "+density)

    val epsChosen = computeMultipleEpsilon(partitionedData, broadcastedSetting, density)

    var i:Int=0
    for( eps <- epsChosen){

      result(i)=(computeMinPoints(partitionedData, broadcastedSetting, eps), eps)

      i+=1
      DBScanPreprocessor.logger.info(i+") Epsilon chosen is "+result(i-1)._2)
      DBScanPreprocessor.logger.info(i+") MinPoints chosen is "+result(i-1)._1)
    }

    result
  }
  
  /*
  private[DBScanPreprocessor] def computeStats(broadcastedSetting:Broadcast[DBSCANPreprocessingSettings],
      input:RDD[(Point, HashMap[Double, Int])],eps:Double) = {
    input.map({
      case (key,value)=>{
        var ptsWithinEps=0
        val keys = value.keysIterator
        while(keys.hasNext){
          val key=keys.next()
          if(key<=eps){
            ptsWithinEps+=value.get(key).get
          }
        }
        (ptsWithinEps)->1
      }
    }).reduceByKey(_+_)
    
  }*/
  
  /**
   * This method computes the estimated epsilon for a given dataset.
   * It works as follows:
   *  - for each point computes the histogram with the number of neighbors
   *    at a given distance;
   *    
   *  - then each histogram is examined to find the greatest decrease in the ratio
   *    numberOfNeighbors/distance and the previous distance value is taken;
   *    
   *  - the values which shows a density below the estimated dataset density value
   *    passed as third parameter are filtered;
   *    
   *  - it is computed the mean of the epsilon values obtained and it is returned.
   * 
   */

  private[DBScanPreprocessor] def computeEpsilon(
      partitionedData:PointsPartitionedByRegionRDD,
      broadcastedSetting:Broadcast[DBSCANPreprocessingSettings],
      minDensity:Double):Double = {
    
    val epsilonRDD = partitionedData.mapPartitions(iter => {
      val seq1 = iter.toArray
      val res = new HashMap[Double, Long]()
      val returned = new HashMap[Double, Long]()
      DBScanPreprocessor.logger.debug("Computing epsilon for "+seq1.size+" points")


      var measure = 0.0
      var modulus = 0.0
      for (a <- seq1 ){
        val dim=a._2.coordinates.length.toDouble
        res.clear()
        for(b <- seq1){
          if(a!=b){
            measure = broadcastedSetting.value.distanceMeasureSuite.distanceMeasure.compute(a._2.coordinates.toArray, b._2.coordinates.toArray)
            modulus=measure%broadcastedSetting.value.epsStep
            measure-=modulus
            if(modulus>0){
              measure+=broadcastedSetting.value.epsStep
            }
            if(measure == 0){
              measure = broadcastedSetting.value.epsStep
            }
            res.put(measure, 1L+res.getOrElse[Long](measure, 0));
          }
        }
        var prevDensity = 0.0
        var aggregate = 0L
        var maxDensityDecrease = 0.0
        var stepChosen = (0.0,0L)
        var prevValue = (0.0,0L)
        if(broadcastedSetting.value.epsilonPolicy==EpsilonPolicies.BeforeMaxDensityDecrease){
          res.toList.sortBy(x=>x._1).foreach( x => {
            aggregate+=x._2
           
            if(aggregate/x._1 - prevDensity < maxDensityDecrease){
              maxDensityDecrease = aggregate/x._1 -prevDensity
              stepChosen = prevValue
            }
            DBScanPreprocessor.logger.debug("At distance "+x._1+" there are "+aggregate+" core points, ie. "+(aggregate/x._1)+" density")
            prevValue = (x._1,aggregate)
            prevDensity = aggregate/x._1
          })
        }else if(broadcastedSetting.value.epsilonPolicy==EpsilonPolicies.BeforeFirstDensityDecrease || broadcastedSetting.value.epsilonPolicy==EpsilonPolicies.BeforeFirstDensityDecrease25){
            var found=false;
            res.toList.sortBy(x=>x._1).foreach( x => {
              if(!found){
                aggregate+=x._2
               
                if(aggregate/Math.pow(x._1,dim) - prevDensity < 0 && aggregate > dim){
                  stepChosen = prevValue
                  found=true
                }
                DBScanPreprocessor.logger.debug("At distance "+x._1+" there are "+aggregate+" core points, ie. "+(aggregate/Math.pow(x._1,dim))+" density")
                prevValue = (x._1,aggregate)
                prevDensity = aggregate/Math.pow(x._1,dim)
              }
            })
        }
        //DBScanPreprocessor.logger.debug("----------------------------------------------")
        /*var maxDensity = 0.0
        var stepChosen = (0.0,0L)
        var aggregate = 0L
        
        res.toList.sortBy(x=>x._1).foreach( x => {
          
          aggregate+=x._2
          if(bool){
            DBScanPreprocessor.logger.debug("Density "+aggregate/x._1+" for "+x._1+"/"+aggregate)
           
          }
          if(aggregate/x._1 > maxDensity){
            maxDensity = aggregate/x._1
            stepChosen = (x._1,aggregate)
          }
          //DBScanPreprocessor.logger.debug("At distance "+x._1+" there are "+aggregate+" core points, ie. "+(aggregate/x._1)+" density")
        })*/
        DBScanPreprocessor.logger.debug("Chosen "+stepChosen._1+" with "+stepChosen._2+" as points.")
        if(stepChosen._1>0 && stepChosen._2/Math.pow(stepChosen._1, dim)>minDensity){
          DBScanPreprocessor.logger.debug("Chosen "+stepChosen._1)
          returned.put(stepChosen._1, 1L+res.getOrElse[Long](stepChosen._1, 0));
        }
      }
      //returned.foreach( x =>{ DBScanPreprocessor.logger.debug("Returned contains: "+x._1+" -> "+x._2) })
      returned.iterator
      
      
    }, true);
    var result=0.0
    settings.epsilonPolicy match {
      case EpsilonPolicies.SquaredMean => {
        val distances = epsilonRDD.aggregate((0.0,0L))((x:(Double,Long),y:(Double,Long))=>
          (x._1+y._1*y._1*y._2, x._2+y._2),
          (x:(Double,Long),y:(Double,Long))=>{
          (x._1+y._1, x._2+y._2)
        })
        result = Math.sqrt(distances._1/distances._2.toDouble)
      }
      case EpsilonPolicies.StdDevOverMean =>{
         val distances = epsilonRDD.aggregate((0.0,0L))((x:(Double,Long),y:(Double,Long))=>
          (x._1+y._1*y._2, x._2+y._2),
          (x:(Double,Long),y:(Double,Long))=>{
          (x._1+y._1, x._2+y._2)
        })
        val mean = distances._1/distances._2.toDouble
        val variance=epsilonRDD.aggregate((0.0,0L))((x:(Double,Long),y:(Double,Long))=>
          (x._1+(y._1-mean)*(y._1-mean)*y._2, x._2+y._2),
          (x:(Double,Long),y:(Double,Long))=>{
          (x._1+y._1, x._2+y._2)
        })
        //  DBScanPreprocessor.logger.debug("Mean "+mean+" stdev"+Math.sqrt(variance._1/variance._2.toDouble))
        result=mean + Math.sqrt(variance._1/variance._2.toDouble)
        
        
      }
      case EpsilonPolicies.BeforeMaxDensityDecrease => {
        val distances = epsilonRDD.aggregate((0.0,0L))((x:(Double,Long),y:(Double,Long))=>
          (x._1+y._1*y._2, x._2+y._2),
          (x:(Double,Long),y:(Double,Long))=>{
          (x._1+y._1, x._2+y._2)
        })
        result = distances._1/distances._2.toDouble - settings.epsStep/2
      }
      case EpsilonPolicies.BeforeFirstDensityDecrease => {
        //epsilonRDD.reduceByKey(_+_).foreach(x => { DBScanPreprocessor.logger.debug("HHH at "+x._1+" there are "+x._2)})
        val distances = epsilonRDD.aggregate((0.0,0L))((x:(Double,Long),y:(Double,Long))=>
          (x._1+y._1*y._2, x._2+y._2),
          (x:(Double,Long),y:(Double,Long))=>{
          (x._1+y._1, x._2+y._2)
        })
        result = distances._1/distances._2.toDouble - settings.epsStep/2
        
      }


      case EpsilonPolicies.BeforeFirstDensityDecrease25 => {

        val cumulate = scala.collection.mutable.Map[Double, Long]()
        val count = epsilonRDD.sortByKey().collect().foldLeft(0L){case (agg, (k, v)) => {cumulate(k) = agg + v; agg +v}}
        val quant25 = cumulate.filter(_._2 >= count/4.0).min._1
        result = quant25

        lazy val median = cumulate.filter(_._2 >= count/2.0).min._1
        lazy val quant75 = cumulate.filter(_._2 >= 3*count/4.0).min._1
        DBScanPreprocessor.logger.debug(s"Epsilons found: [$quant25, $median, $quant75] (1st quart, median, 3rd quart)")

      }
    }
    
    
    
    
    
     result
    
  }


  private[DBScanPreprocessor] def computeMultipleEpsilon(
                                                  partitionedData:PointsPartitionedByRegionRDD,
                                                  broadcastedSetting:Broadcast[DBSCANPreprocessingSettings],
                                                  minDensity:Double):Array[Double] = {

    val epsilonRDD = partitionedData.mapPartitions(iter => {
      val seq1 = iter.toArray
      val res = new HashMap[Double, Long]()
      val returned = new HashMap[Double, Long]()

      DBScanPreprocessor.logger.debug("Computing epsilon for "+seq1.size+" points")
      var measure = 0.0
      var modulus = 0.0
      for (a <- seq1 ){
        val dim=a._2.coordinates.length.toDouble
        res.clear()
        for(b <- seq1){
          if(a!=b){
            measure = broadcastedSetting.value.distanceMeasureSuite.distanceMeasure.compute(a._2.coordinates.toArray, b._2.coordinates.toArray)
            modulus=measure%broadcastedSetting.value.epsStep
            measure-=modulus
            if(modulus>0){
              measure+=broadcastedSetting.value.epsStep
            }
            if(measure == 0){
              measure = broadcastedSetting.value.epsStep
            }
            res.put(measure, 1L+res.getOrElse[Long](measure, 0));
          }
        }
        var prevDensity = 0.0
        var aggregate = 0L
        var maxDensityDecrease = 0.0
        var stepChosen = (0.0,0L)
        var prevValue = (0.0,0L)

        if(broadcastedSetting.value.epsilonPolicy==EpsilonPolicies.BeforeMaxDensityDecrease){
          res.toList.sortBy(x=>x._1).foreach( x => {
            aggregate+=x._2

            if(aggregate/x._1 - prevDensity < maxDensityDecrease){
              maxDensityDecrease = aggregate/x._1 -prevDensity
              stepChosen = prevValue
            }
            DBScanPreprocessor.logger.debug("At distance "+x._1+" there are "+aggregate+" core points, ie. "+(aggregate/x._1)+" density")
            prevValue = (x._1,aggregate)
            prevDensity = aggregate/x._1
          })
        }else if(broadcastedSetting.value.epsilonPolicy==EpsilonPolicies.BeforeFirstDensityDecrease || broadcastedSetting.value.epsilonPolicy==EpsilonPolicies.BeforeFirstDensityDecrease25){
          var found=false;
          res.toList.sortBy(x=>x._1).foreach( x => {
            if(!found){
              aggregate+=x._2

              if(aggregate/Math.pow(x._1,dim) - prevDensity < 0 && aggregate > dim){
                stepChosen = prevValue
                found=true
              }
              DBScanPreprocessor.logger.debug("At distance "+x._1+" there are "+aggregate+" core points, ie. "+(aggregate/Math.pow(x._1,dim))+" density")

              prevValue = (x._1,aggregate)
              prevDensity = aggregate/Math.pow(x._1,dim)
            }
          })
        }

        //DBScanPreprocessor.logger.debug("----------------------------------------------")
        /*var maxDensity = 0.0
        var stepChosen = (0.0,0L)
        var aggregate = 0L

        res.toList.sortBy(x=>x._1).foreach( x => {

          aggregate+=x._2
          if(bool){
            DBScanPreprocessor.logger.debug("Density "+aggregate/x._1+" for "+x._1+"/"+aggregate)

          }
          if(aggregate/x._1 > maxDensity){
            maxDensity = aggregate/x._1
            stepChosen = (x._1,aggregate)
          }
          //DBScanPreprocessor.logger.debug("At distance "+x._1+" there are "+aggregate+" core points, ie. "+(aggregate/x._1)+" density")
        })*/
        DBScanPreprocessor.logger.debug("Chosen "+stepChosen._1+" with "+stepChosen._2+" as points.")
        if(stepChosen._1>0 && stepChosen._2/Math.pow(stepChosen._1, dim)>minDensity){
          DBScanPreprocessor.logger.debug("Chosen "+stepChosen._1)
          returned.put(stepChosen._1, 1L+res.getOrElse[Long](stepChosen._1, 0));
        }
      }
      //returned.foreach( x =>{ DBScanPreprocessor.logger.debug("Returned contains: "+x._1+" -> "+x._2) })
      returned.iterator


    }, true);
    var result=Array(0.0)
    settings.epsilonPolicy match {
      case EpsilonPolicies.SquaredMean => {
        val distances = epsilonRDD.aggregate((0.0,0L))((x:(Double,Long),y:(Double,Long))=>
          (x._1+y._1*y._1*y._2, x._2+y._2),
          (x:(Double,Long),y:(Double,Long))=>{
            (x._1+y._1, x._2+y._2)
          })
        result = Array(Math.sqrt(distances._1/distances._2.toDouble))
      }
      case EpsilonPolicies.StdDevOverMean =>{
        val distances = epsilonRDD.aggregate((0.0,0L))((x:(Double,Long),y:(Double,Long))=>
          (x._1+y._1*y._2, x._2+y._2),
          (x:(Double,Long),y:(Double,Long))=>{
            (x._1+y._1, x._2+y._2)
          })
        val mean = distances._1/distances._2.toDouble
        val variance=epsilonRDD.aggregate((0.0,0L))((x:(Double,Long),y:(Double,Long))=>
          (x._1+(y._1-mean)*(y._1-mean)*y._2, x._2+y._2),
          (x:(Double,Long),y:(Double,Long))=>{
            (x._1+y._1, x._2+y._2)
          })
        //  DBScanPreprocessor.logger.debug("Mean "+mean+" stdev"+Math.sqrt(variance._1/variance._2.toDouble))
        result= Array(mean + Math.sqrt(variance._1/variance._2.toDouble))


      }
      case EpsilonPolicies.BeforeMaxDensityDecrease => {
        val distances = epsilonRDD.aggregate((0.0,0L))((x:(Double,Long),y:(Double,Long))=>
          (x._1+y._1*y._2, x._2+y._2),
          (x:(Double,Long),y:(Double,Long))=>{
            (x._1+y._1, x._2+y._2)
          })
        result = Array(distances._1/distances._2.toDouble - settings.epsStep/2)
      }
      case EpsilonPolicies.BeforeFirstDensityDecrease => {
        //epsilonRDD.reduceByKey(_+_).foreach(x => { DBScanPreprocessor.logger.debug("HHH at "+x._1+" there are "+x._2)})
        val distances = epsilonRDD.aggregate((0.0,0L))((x:(Double,Long),y:(Double,Long))=>
          (x._1+y._1*y._2, x._2+y._2),
          (x:(Double,Long),y:(Double,Long))=>{
            (x._1+y._1, x._2+y._2)
          })
        result = Array(distances._1/distances._2.toDouble - settings.epsStep/2)

      }


      case EpsilonPolicies.BeforeFirstDensityDecrease25 => {

        val cumulate = scala.collection.mutable.Map[Double, Long]()
        val count = epsilonRDD.sortByKey().collect().foldLeft(0L){case (agg, (k, v)) => {cumulate(k) = agg + v; agg +v}}
        val quant25 = cumulate.filter(_._2 >= count/4.0).min._1
        val median = cumulate.filter(_._2 >= count/2.0).min._1
        val quant75 = cumulate.filter(_._2 >= 3*count/4.0).min._1
        DBScanPreprocessor.logger.debug("Epsilons found:"+epsilonRDD.sortByKey().map(_.toString()).collect().mkString(":"))
        DBScanPreprocessor.logger.debug(s"Epsilons found: [$quant25, $median, $quant75] (1st quart, median, 3rd quart)")

        result = Array(quant25, median, quant75)

      }
    }





    result
  }



  /**
   * This method computes the estimated minPoints for a given dataset.
   * It works as follows:
   *  - for each point computes the number of neighbors (only in the same partition)
   *    within the epsilon provided;
   *    
   *  - then the values are aggregated to an histogram;
   *    
   *  - the value which maximize estimatedCorePoints*value is returned
   * 
   */
  private[DBScanPreprocessor] def computeMinPoints(
      partitionedData:PointsPartitionedByRegionRDD,
      broadcastedSetting:Broadcast[DBSCANPreprocessingSettings],
      eps:Double):Int = {

    var aggregate = 0L
    var currMinPts=0
    var currValue = 0.0
    var prevValue=0
    
    
    
    val histogram = partitionedData.mapPartitions(iter => {
      val seq1 = iter.toArray
      val returned = new HashMap[Int, Long]()
      var count:Int=0
      for (a <- seq1 ){
        
        for(b <- seq1){
          if(a!=b){
             if(broadcastedSetting.value.distanceMeasureSuite.distanceMeasure.compute(a._2.coordinates.toArray, b._2.coordinates.toArray)<=eps){
               count+=1
             }
          }
        }
        
        if(count>0){
            returned.put(count, 1L+returned.getOrElse[Long](count, 0));
            count=0
          }
      }
      //returned.foreach( x =>{ DBScanPreprocessor.logger.debug("Returned contains: "+x._1+" -> "+x._2) })
      returned.iterator
      
      
    })
    .reduceByKey(_+_)
    .collect();
    //val hDerivata = new Array[(Long, Double)](histogram.length-1);
    //var index= -1
    
    settings.minPtsPolicy match  {
      case MinPointsPolicies.MyGreedyFunc => {
        
        histogram.sortBy(x=>(-x._1)).foreach(nPts =>{
          
          aggregate+=nPts._2
          DBScanPreprocessor.logger.debug("Histogram for minPts : at "+nPts._1+" there are "+aggregate+" approximated core points.")
         
          if(aggregate*nPts._1>currValue){
            currMinPts = nPts._1
            currValue = aggregate*nPts._1
          }
        })
      }
      case MinPointsPolicies.MinDecrease => {
       
        histogram.sortBy(x=>(-x._1))
        .foreach( nPts =>{
          if(prevValue>0){
            if((-nPts._2.toDouble/(prevValue.toDouble-nPts._1.toDouble))>=currValue){
              currMinPts =prevValue.toInt
              currValue = (-nPts._2.toDouble/(prevValue.toDouble-nPts._1.toDouble))
            }
          }else{
            currValue = Double.MinValue
          }
          prevValue = nPts._1
        })
      }
      case MinPointsPolicies.BeforeGreatestPercentageIncrement =>{
        
        val sorted = histogram.sortBy(x=>(-x._1))
        var threeStepIncrement=0L
        
        for(i <- 1 to sorted.length-4){
          aggregate+=sorted.apply(i-1)._2
          if(aggregate>10){
            threeStepIncrement = sorted.apply(i)._2 + sorted.apply(i+1)._2 + sorted.apply(i+2)._2
            if((threeStepIncrement+aggregate).toDouble/aggregate.toDouble>currValue){
              currValue=(threeStepIncrement+aggregate).toDouble/aggregate.toDouble
              currMinPts=sorted(i)._1
            }
          }
          
          
        }
      } 
    }
    
    
    
//    .foreach(nPts =>{
//      
//      aggregate+=nPts._2
//      //DBScanPreprocessor.logger.debug("Histogram for minPts : at "+nPts._1+" there are "+aggregate+" approximated core points.")
//      //println(prevValue+"/"+(-nPts._2.toDouble/(prevValue.toDouble-nPts._1.toDouble)))
//      if(index>=0){
//        hDerivata(index) = (prevValue, -nPts._2.toDouble/(prevValue.toDouble-nPts._1.toDouble))
//      }
//      if((-nPts._2.toDouble/(prevValue.toDouble-nPts._1.toDouble))<currValue){
//      //if(aggregate*nPts._1>currValue){
//        //currMinPts = nPts._1
//        //currValue = aggregate*nPts._1
//        currMinPts =prevValue.toInt
//        currValue = (-nPts._2.toDouble/(prevValue.toDouble-nPts._1.toDouble))
//      }
//      prevValue = nPts._1
//      index+=1
//    })
//    hDerivata.foreach(x=>{
//      DBScanPreprocessor.logger.debug(x._1+" : derivata "+x._2)
//    })
//    var prevDerivata2=(hDerivata(1)._2-hDerivata(0)._2)/(hDerivata(1)._1-hDerivata(0)._1)
//    for(i<-2 to hDerivata.length-1){
//      DBScanPreprocessor.logger.debug("Derivata 2 in "+hDerivata(i-1)._1+" is "+prevDerivata2)
//      if((hDerivata(i)._2-hDerivata(i-1)._2)/(hDerivata(i)._1-hDerivata(i-1)._1)*prevDerivata2<=0){
//        //DBScanPreprocessor.logger.debug("Flex in "+hDerivata(i-1)._1)
//      }
//      prevDerivata2=(hDerivata(i)._2-hDerivata(i-1)._2)/(hDerivata(i)._1-hDerivata(i-1)._1)
//    }
 
    currMinPts
  }

  @deprecated
  private[DBScanPreprocessor] def computeMultipleEpsilonOld(
      partitionedData:PointsPartitionedByRegionRDD,
      broadcastedSetting:Broadcast[DBSCANPreprocessingSettings],
      minDensity:Double):Array[Double] = {
    
    var suggested =  new Array [Double] (broadcastedSetting.value.maxSuggestedEpsilon)
    
    val distances = partitionedData.mapPartitions(iter => {
      val seq1 = iter.toArray
      val res = new HashMap[Double, Long]()
      val returned = new HashMap[Double, Long]()
      
      var measure = 0.0
      var modulus = 0.0
      for (a <- seq1 ){
        val dim=a._2.coordinates.length.toDouble
        res.clear()
        for(b <- seq1){
          if(a!=b){
            measure = broadcastedSetting.value.distanceMeasureSuite.distanceMeasure.compute(a._2.coordinates.toArray, b._2.coordinates.toArray)
            modulus=measure%broadcastedSetting.value.epsStep
            measure-=modulus
            if(modulus>0){
              measure+=broadcastedSetting.value.epsStep
            }
            res.put(measure, 1L+res.getOrElse[Long](measure, 0));
          }
        }
        var prevDensity = 0.0
        var aggregate = 0.0
        var maxDensityDecrease = 0.0
        var stepChosen = (0.0,0L)
        var prevValue = (0.0,0L)
        
        res.toList.sortBy(x=>x._1).foreach( x => {
          aggregate+=x._2
         
          if(aggregate/x._1 - prevDensity < maxDensityDecrease){
            maxDensityDecrease = aggregate/x._1 -prevDensity
            stepChosen = prevValue
          }
          
          prevValue = x
          prevDensity = aggregate/x._1
        })
        
        
        if(stepChosen._1>0 && stepChosen._2/Math.pow(stepChosen._1, dim)>minDensity){
          //DBScanPreprocessor.logger.debug("Chosen "+stepChosen._1)
          returned.put(stepChosen._1, 1L+res.getOrElse[Long](stepChosen._1, 0));
        }
      }
      returned.iterator
      
    })
    .reduceByKey( _+_ )
    .collect()
    
    var prevDensity = 0.0
    var aggregate = 0.0
    var maxDensityDecrease = 0.0
    var prevValue = 0.0
    var index:Int = 0
    
    distances.sortBy(x=>x._1).foreach({
      case (epsCurrent, countCurrent) => {
        aggregate+=countCurrent
        DBScanPreprocessor.logger.debug(aggregate+" points chose distance "+epsCurrent+" ")
        if(aggregate/epsCurrent - prevDensity < maxDensityDecrease){
          maxDensityDecrease = aggregate/epsCurrent -prevDensity
          suggested(index) = prevValue
        }else{
          if(aggregate/epsCurrent - prevDensity>0 && maxDensityDecrease<0){
            maxDensityDecrease=0
            index+=1
            if(index==broadcastedSetting.value.maxSuggestedEpsilon){
              index=0
            }
          }
        }
        
        
        prevValue = epsCurrent
        prevDensity = aggregate/epsCurrent
        
      }
    })
    
    suggested
   
    
  }

  @deprecated //use stateless normalize(points)
  def normalize() = {
    settings.normalizationTechnique match {
      case NormalizationTechnique.Zscore => normalizeZScore();
      case NormalizationTechnique.MinMax => normalizeMinMax();
      case NormalizationTechnique.None => data
      case _ => throw new UnimplementedException();
      
    }
  }

  def normalize(points:RDD[Point]) = {
    settings.normalizationTechnique match {
      case NormalizationTechnique.Zscore => normalizeZScore(points);
      case NormalizationTechnique.MinMax => normalizeMinMax(points);
      case NormalizationTechnique.None => points
      case _ => throw new UnimplementedException();

    }
  }

  def denormalize(points:RDD[Point]) = {
    settings.normalizationTechnique match {
      case NormalizationTechnique.Zscore => denormalizeZScore(points);
      case NormalizationTechnique.MinMax => denormalizeMinMax(points);
      case NormalizationTechnique.None => points
      case _ => throw new UnimplementedException();

    }
  }
  private[DBScanPreprocessor] def normalizeMinMax() = {

    DBScanPreprocessor.logger.debug("[min-max normalization] mins : "+minAndMax.map(x => x._1).mkString(";"))
    DBScanPreprocessor.logger.debug("[min-max normalization] maxs : "+minAndMax.map(x => x._2).mkString(";"))
    val broadcastedMinMax = sc.broadcast(minAndMax)
    
    data = data.map { x => new Point(
        x.coordinates.zip(broadcastedMinMax.value)
        .map( t => 
          if(t._2._2==t._2._1) 1 else (t._1-t._2._1.toDouble)/(t._2._2-t._2._1).toDouble
          ).toArray) }
    data
          
  }

  private[DBScanPreprocessor] def normalizeMinMax(data:RDD[Point]) = {

    DBScanPreprocessor.logger.debug("[min-max normalization] mins : "+minAndMax.map(x => x._1).mkString(";"))
    DBScanPreprocessor.logger.debug("[min-max normalization] maxs : "+minAndMax.map(x => x._2).mkString(";"))
    val broadcastedMinMax = sc.broadcast(minAndMax)

    data.map { x => new Point(
      x.coordinates.zip(broadcastedMinMax.value)
        .map( t =>
          if(t._2._2==t._2._1) 1 else (t._1-t._2._1.toDouble)/(t._2._2-t._2._1).toDouble
        ).toArray) }


  }

  private[DBScanPreprocessor] def denormalizeMinMax(points:RDD[Point]) = {

    DBScanPreprocessor.logger.debug("[min-max normalization] mins : "+minAndMax.map(x => x._1).mkString(";"))
    DBScanPreprocessor.logger.debug("[min-max normalization] maxs : "+minAndMax.map(x => x._2).mkString(";"))
    val broadcastedMinMax = sc.broadcast(minAndMax)

    val res = points.map { x => new Point(
      x.coordinates.zip(broadcastedMinMax.value)
        .map( t =>
          t._1*((t._2._2-t._2._1).toDouble)+t._2._1.toDouble
        ).toArray).withClusterId(x.clusterId) }
    res

  }
  
  private[DBScanPreprocessor] def normalizeZScore() = {

    val broadcastedMeans = sc.broadcast(zDistribution.means)
    val broadcastedStdDevs = sc.broadcast(zDistribution.standardDeviations)
    
    data = data.map ( p =>  {
      new Point(p.coordinates.zip(broadcastedMeans.value).zip(broadcastedStdDevs.value).map(
            t => if(t._2==0) 1 else (t._1._1-t._1._2)/t._2
        ).toArray)
      
      
      
      
    })
    data
  }

  private[DBScanPreprocessor] def normalizeZScore(data:RDD[Point]) = {

    val broadcastedMeans = sc.broadcast(zDistribution.means)
    val broadcastedStdDevs = sc.broadcast(zDistribution.standardDeviations)

    data.map ( p =>  {
      new Point(p.coordinates.zip(broadcastedMeans.value).zip(broadcastedStdDevs.value).map(
        t => if(t._2==0) 1 else (t._1._1-t._1._2)/t._2
      ).toArray)




    })

  }

  private[DBScanPreprocessor] def denormalizeZScore(points:RDD[Point]) = {

    val broadcastedMeans = sc.broadcast(zDistribution.means)
    val broadcastedStdDevs = sc.broadcast(zDistribution.standardDeviations)

    val res = points.map ( p =>  {
      new Point(p.coordinates.zip(broadcastedMeans.value).zip(broadcastedStdDevs.value).map(
        t =>  t._2*t._1._1+t._1._2
      ).toArray).withClusterId(p.clusterId)
    })
    res
  }
  
  /*private[DBScanPreprocessor]*/ def partitioning():PointsPartitionedByRegionRDD = {
    var partitionedData = PointsPartitionedByRegionRDD(data, 
                                new PartitioningSettings(2,16,settings.numberOfPointsPerPartition,1,1), 
                                new DbscanSettings()
                                  .withDistanceMeasureSuite(settings.distanceMeasureSuite)
                                  .withEpsilon(settings.epsStep)
                            )
    /*val initialData = data
    
    val listOfPartitionsToBeRem=ListBuffer[Int]()
    do{
      val initial_count = data.count()
      listOfPartitionsToBeRem.clear()
      val countPerPartition = partitionedData.mapPartitionsWithIndex((i,x)=>{
        x.map(x=>(i,1L))
      }).foldByKey(0L)(_+_).toLocalIterator
       
      countPerPartition.foreach(count => {
        if(count._2 < initial_count/10000){ //TODO settare un valore sensato
          listOfPartitionsToBeRem+=count._1
        }
      })
      val lastFiltered=partitionedData
              .mapPartitionsWithIndex((i,p)=>p.map(t=>(i,t._2)), true)
              .filter(p => listOfPartitionsToBeRem.contains(p._1))
              .map(p => p._2)
              
      DBScanPreprocessor.logger.debug("Filtered "+lastFiltered.count()+" points")
      
      //filteredData=filteredData.union(lastFiltered)

      //DBScanPreprocessor.logger.debug("Filtered so far "+filteredData.count()+" points")
      
      data = data.subtract(lastFiltered)
      
      //normalize()
      
      partitionedData = PointsPartitionedByRegionRDD(data, 
                                new PartitioningSettings(2,16,settings.numberOfPointsPerPartition,2,10), 
                                new DbscanSettings()
                                  .withDistanceMeasureSuite(settings.distanceMeasureSuite)
                                  .withEpsilon(settings.epsStep)
                            )
    }while(listOfPartitionsToBeRem.size>0)
    
    filteredData = initialData.subtract(data)
    
    DBScanPreprocessor.logger.info("Preprocessing has filtered "+filteredData.count()+" points")*/
    partitionedData
    
  }
  
}