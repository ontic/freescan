package it.polito.dbdmg.ontic.postprocessing.cosine

/**
 * Class representing a point, with some additional infos
 * useful for the squared silhouette computation.
 */
class CosineSilhouetteNode (val coordinates:Array[Double], val clusterId:Int) extends Serializable {
  
  /**
   * This value contains the sum 
   * of the squares of the coordinates of the point,
   * but it is not initialized: it has to be initialized
   * with the computeCsi method of the 
   * [[it.polito.dbdmg.ontic.postprocessing.Node]] object.
   * 
   * This is basically the square of the norm of the vector.
   * 
   */
  private[CosineSilhouetteNode] var csi:Array[Double] = Array()
  
  def getCsi() = {
    assert ( csi.length == coordinates.length)
    csi
  }
  
  
}

object CosineSilhouetteNode {
  
  def apply(coordinates:Array[Double], clusterId:Int, csi:Array[Double]) = {
    val n=new CosineSilhouetteNode(coordinates, clusterId)
    n.csi = csi
    n
  }
  
  def computeCsi(coordinates:Array[Double]):Array[Double] = {
    var sum=0.0
    for(i <- coordinates){
      sum+=i*i
    }
    val norm = math.sqrt(sum)
    coordinates.map { x => x / norm }
  }
  
}

