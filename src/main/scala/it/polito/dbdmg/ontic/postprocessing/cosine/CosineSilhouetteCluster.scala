package it.polito.dbdmg.ontic.postprocessing.cosine

import org.apache.spark.rdd.RDD
import org.apache.spark.SparkContext._
import org.apache.log4j.Logger

class CosineSilhouetteCluster(val clusterId:Int) extends Serializable {
  
  private[CosineSilhouetteCluster] var count:Long = 0L
  private[CosineSilhouetteCluster] var omega:Array[Double] = Array()
  
  def getOmega() = {
    assert(omega.length >0)
    omega
  }
  
  def getCount() = {
    assert(count > 0)
    count
  }
  
  
}

object CosineSilhouetteCluster{
  
  private[CosineSilhouetteCluster] val logger = Logger.getLogger("it.polito.dbdmg.ontic.Logger")
  
  private[CosineSilhouetteCluster] def apply(clusterId:Int, omega:Array[Double], count:Long) = {
    val c = new CosineSilhouetteCluster(clusterId)
    c.omega=omega
    c.count=count
    c
  }
  
  private[CosineSilhouetteCluster] def generateClustersComputingOmegaAndCount(points:RDD[(Int,CosineSilhouetteNode)]) = {
    val d = points.first()._2.coordinates.length
    points.aggregateByKey((Array.fill[Double](d)(0),0L))((a,b)=>(a._1.zip(b.getCsi()).map( x => x._1+x._2 ),a._2+1), (a,b)=>(a._1.zip(b._1).map( x => x._1+x._2 ),a._2+b._2))
      .collectAsMap().map(tuple => { 
        //val c = new Cluster(tuple._1)
        //c.psi = tuple._2
        
        (tuple._1,CosineSilhouetteCluster(tuple._1, tuple._2._1, tuple._2._2))
      }
    )
  }
  
  
  
  private[CosineSilhouetteCluster] def mapPoints(points:RDD[CosineSilhouetteNode])={points.map(a=>(a.clusterId,a))}
  
  /**
   * This method returns a map which associates each cluster id
   * to the relative [[it.polito.dbdmg.ontic.postprocessing.Cluster]]
   * object containing all the needed and precomputed informations
   * for the [[it.polito.dbdmg.ontic.postprocessing.SquaredSilhouette]].
   */
  def getClustersMap(points:RDD[CosineSilhouetteNode]):scala.collection.Map[Int, CosineSilhouetteCluster] = {
    val nodes = mapPoints(points)
    generateClustersComputingOmegaAndCount(nodes)
  }
  
}