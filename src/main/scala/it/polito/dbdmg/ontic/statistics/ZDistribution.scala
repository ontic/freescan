package it.polito.dbdmg.ontic.statistics

import org.apache.spark.rdd.RDD
import org.apache.log4j.Logger

class ZDistribution(val means:Array[Double], val standardDeviations:Array[Double]) {
  
  
  
  
  
}

object ZDistribution{
  
  val logger = Logger.getLogger("it.polito.dbdmg.ontic.Logger")
  
  def apply(data:RDD[Array[Double]]):ZDistribution = {
    
    val count:Long=data.count()
    
    // sums._1 contains the sum of all the values, while sums._2 contains the sum
    // of all squared values
    val sums = data.aggregate(Array.fill(data.first().length)(BigDecimal(0.0), BigDecimal(0.0)))(
          (sums,current) => {
            sums.zip(current).map((x)=>(x._1._1+x._2, x._1._2+x._2*x._2))
          }, 
          (sums1, sums2) => {
            sums1.zip(sums2).map({
              case (a, b) => (a._1+b._1, a._2+b._2)
            })
          })
    // array containing the means of all dimensions
    val means = sums.map( x => (x._1 / count).toDouble )
    val squaredMeans = sums.map( x => (x._2 / count).toDouble )
    
    ZDistribution.logger.debug("[ZDistribution] means : "+means.mkString(";"))
    ZDistribution.logger.debug("[ZDistribution] squared means : "+squaredMeans.mkString(";"))
    
    // standard deviations computed using the following formula for the variance:
    //    var(X) = E[X2]-E[X]2
    // which reduces the computation errors
    val stdDevs = squaredMeans.zip(means).map(
        {case (squared,m) => Math.sqrt( (squared - m*m).toDouble ) }
    )
    ZDistribution.logger.debug("[Z-Score nromalization] standard deviations : "+stdDevs.mkString(";"))

    new ZDistribution(means, stdDevs)
  }
  
}