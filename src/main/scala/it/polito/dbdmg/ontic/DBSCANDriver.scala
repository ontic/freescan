package it.polito.dbdmg.ontic

import it.polito.dbdmg.ontic.util.CommandLineParser
import org.alitouka.spark.dbscan
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.log4j.{PatternLayout, FileAppender, RollingFileAppender, Logger}
import it.polito.dbdmg.ontic.io.ParseSettings
import it.polito.dbdmg.ontic.io.IOHandler
import it.polito.dbdmg.ontic.preprocessing.DBSCANPreprocessingSettings
import it.polito.dbdmg.ontic.preprocessing.DBScanPreprocessor
import org.alitouka.spark.dbscan.DbscanSettings
import org.alitouka.spark.dbscan.Dbscan
import org.alitouka.spark.dbscan.spatial.Point
import org.alitouka.spark.dbscan.DbscanModel
import org.apache.spark.rdd.RDD



object FuzzyPoint {
  def roundAt(p: Int)(n: Double): Double = { val s = math pow (10, p); (math round n * s) / s }

  def transform (coord : dbscan.PointCoordinates) : Array[Double] = {
    val roundAt1 = roundAt(0) _
    coord.map(x => roundAt1(x)).toArray
  }
}

object DBSCANDriver {
  
  def main(args: Array[String]): Unit = {
    //parsing arguments
    val clArgs:CommandLineParser = new CommandLineParser("Ontic Spark DBSCAN")
    if(clArgs.parse(args)){
  
      //setting log levels
      val logger = Logger.getLogger("it.polito.dbdmg.ontic.Logger")
      
      logger.setLevel(clArgs.args.onticLogLevel)
      Logger.getRootLogger.setLevel(clArgs.args.sparkLogLevel)
      val layout = new PatternLayout("%d{ISO8601} [%t] %-5p %c %x - %m%n")
      val fileappender = new FileAppender(layout, clArgs.args.outputPath+"/dbscan.log")

      logger.addAppender(fileappender)
      
      //creating spark context
      var conf=new SparkConf().setAppName("Ontic Spark DBSCAN")
      if(clArgs.args.debugOutputPath.isDefined)              
         conf=conf.set("DebugOutputPath", clArgs.args.debugOutputPath.get)
         
      logger.debug("Spark master: " +clArgs.args.masterUrl)
         
      
      val sc=new SparkContext(conf)
      
      //set up parsing settings
      val parseSettings = new ParseSettings()
                               .withSeparator(clArgs.args.parserSeparator)
                               .withParser(clArgs.args.parserClassName)
      
      logger.debug("Parse settings:\r\n\tseparator : " +parseSettings.textFileParser.separator+"\r\n\tclass : " +parseSettings.textFileParser.getClass.getCanonicalName )
      
      //set up preprocessing settings
      val preprocessingSettings =  new DBSCANPreprocessingSettings()
                                          .withEpsilonPolicy(clArgs.args.epsilonPolicy.toString())
                                          .withEpsStep(clArgs.args.epsilonStep)
                                          .withDistanceMeasureSuite(clArgs.args.distanceMeasureSuite.getClass.getCanonicalName)
                                          .withMinPointsPolicy(clArgs.args.minPtsPolicy.toString())
                                          .withNormalizationTechnique(clArgs.args.normalizationTechnique.toString())
                                          //.withNumberOfPointsPerPartition(n)
      logger.debug("Epsilon policy : "+preprocessingSettings.epsilonPolicy)
      logger.debug("Min points policy : "+preprocessingSettings.minPtsPolicy)
      logger.debug("Epsilon step : "+preprocessingSettings.epsStep)
      logger.debug("Distance measure suite : "+preprocessingSettings.distanceMeasureSuite.getClass.getCanonicalName)
      logger.debug("Normalization technique : "+preprocessingSettings.normalizationTechnique)
      
      //var data = IOHandler.readTextFile(sc, parseSettings,clArgs.args.inputPath)
      val parse = parseSettings.textFileParser.parse _
      val point2string = sc.textFile(clArgs.args.inputPath).map{x => (parse(x), x)}
      var data = point2string.keys


      sc.parallelize(data.take(10)).saveAsTextFile(clArgs.args.outputPath+"/sample.dat") //check write permission on output folder
      
      if(preprocessingSettings.distanceMeasureSuite.isInstanceOf[org.alitouka.spark.dbscan.spatial.CosineAngleDistanceSuite]){
        data = data.filter { x => x.coordinates.exists { x => x!=0.0 } }
      }
      
      val normalizer = new DBScanPreprocessor(sc, data, preprocessingSettings)
      
      data = normalizer.normalize()
      data.persist()

      if(clArgs.args.hierarchical){
        val preprocessor = normalizer
        val startingTime = System.currentTimeMillis()
        val params = preprocessor.preprocessMultiple()
        //to get rid of filtered data
        data = preprocessor.data
        val endPreprocessTime = System.currentTimeMillis()
        logger.info("Total time to preprocess data: " + (endPreprocessTime - startingTime) / 1000 + " s")

        var i=1
        val minPts = params(0)._1
        for((_, eps) <- params){
          val clusteringSettings = new DbscanSettings()
            .withEpsilon(eps).withNumberOfPoints(minPts + 1)
            .withDistanceMeasureSuite(clArgs.args.distanceMeasureSuite)

          logger.debug("At step " + i + ", settings chosen are " + minPts + " as minPoints and " + eps + " as eps")

          val model = Dbscan.train(data, clusteringSettings)
          //to add filtered data
          model.allPoints.union(preprocessor.filteredData.map(p => new Point(p.coordinates).withClusterId(DbscanModel.NoisePoint)))

          val realPoints = preprocessor.denormalize(model.allPoints)

          val borderPoints = model.clusteredPoints.filter(!isCorePoint(_, model.settings))
          val realBorderPoints = normalizer.denormalize(borderPoints)
          val fuzzyStartingPoints = point2string.
            map { case (p, s) => (new Point(FuzzyPoint.transform(p.coordinates)), s) }
          val fuzzyRealPoints = realPoints.
            map(x => new Point(FuzzyPoint.transform(x.coordinates)).withClusterId(x.clusterId)).
            distinct().
            map(x => (x, x.clusterId.toLong))
          val originalPoints = fuzzyStartingPoints.join(fuzzyRealPoints).values

          IOHandler.saveClusteringResult(realPoints, clArgs.args.outputPath + "/denorm/" + i)
          IOHandler.saveClusteringResult(model, clArgs.args.outputPath + "/norm/" + i)
          IOHandler.saveClusteringResult(realBorderPoints, clArgs.args.outputPath + "/border_denorm/" + i)
          IOHandler.saveClusteringResult(model, clArgs.args.outputPath + "/border_norm/" + i)
          originalPoints.map{case (s, cl) => s+"\t"+cl}.saveAsTextFile(clArgs.args.outputPath + "/results/" + i)
          logger.info("Total time to process: " + (System.currentTimeMillis() - endPreprocessTime) / 1000 + " s")

          logger.info("Count of noise points is " + model.noisePoints.count())
          i+=1
        }
      } else {
        var preprocessor = normalizer
        for (i <- 1 to clArgs.args.numberOfClusteringLevels) {
          val startingTime = System.currentTimeMillis()
          val (minPts, eps) = preprocessor.preprocess()
          //to get rid of filtered data
          data = preprocessor.data
          val endPreprocessTime = System.currentTimeMillis()
          logger.info("Total time to preprocess data: " + (endPreprocessTime - startingTime) / 1000 + " s")

          val clusteringSettings = new DbscanSettings()
            .withEpsilon(eps).withNumberOfPoints(minPts + 1)
            .withDistanceMeasureSuite(clArgs.args.distanceMeasureSuite)

          logger.debug("At step " + i + ", settings chosen are " + minPts + " as minPoints and " + eps + " as eps")

          val model = Dbscan.train(data, clusteringSettings)
          //to add filtered data
          model.allPoints.union(preprocessor.filteredData.map(p => new Point(p.coordinates).withClusterId(DbscanModel.NoisePoint)))

          val realPoints = normalizer.denormalize(model.allPoints)

          val borderPoints = model.clusteredPoints.filter(!isCorePoint(_, model.settings))
          val realBorderPoints = normalizer.denormalize(borderPoints)
          val fuzzyStartingPoints = point2string.
            map { case (p, s) => (new Point(FuzzyPoint.transform(p.coordinates)), s) }
          val fuzzyRealPoints = realPoints.
            map(x => new Point(FuzzyPoint.transform(x.coordinates)).withClusterId(x.clusterId)).
            distinct().
            map(x => (x, x.clusterId.toLong))
          val originalPoints = fuzzyStartingPoints.join(fuzzyRealPoints).values


          IOHandler.saveClusteringResult(realPoints, clArgs.args.outputPath + "/denorm/" + i)
          IOHandler.saveClusteringResult(model, clArgs.args.outputPath + "/norm/" + i)
          IOHandler.saveClusteringResult(realBorderPoints, clArgs.args.outputPath + "/border_denorm/" + i)
          IOHandler.saveClusteringResult(borderPoints, clArgs.args.outputPath + "/border_norm/" + i)
          originalPoints.map{case (s, cl) => s+"\t"+cl}.saveAsTextFile(clArgs.args.outputPath + "/results/" + i)

          logger.info("Total time to process: " + (System.currentTimeMillis() - endPreprocessTime) / 1000 + " s")

          //update data
          data = model.noisePoints.map { p => new Point(p.coordinates) }
          data.persist()
          preprocessor = new DBScanPreprocessor(sc, data, preprocessingSettings)
          logger.info("Count of noise points is " + data.count())
        }
      }
    }
  }

  private def isCorePoint (pt: Point, settings: DbscanSettings): Boolean = {
    pt.precomputedNumberOfNeighbors >= settings.numberOfPoints
  }
}