package it.polito.dbdmg.ontic

import it.polito.dbdmg.ontic.io.{IOHandler, ClusteredIOParser}
import it.polito.dbdmg.ontic.util.SilhouetteCommandLineParser
import it.polito.dbdmg.ontic.postprocessing.BorderSilhouette
import org.apache.log4j.Logger
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.SparkContext._
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.linalg.Vector

import scala.collection.immutable.IndexedSeq
import scala.collection.mutable.ListBuffer

/**
 * @author mark9
 */
object BorderSilhouetteDriver {
  
  def main(args:Array[String]):Unit = {
    val clArgs:SilhouetteCommandLineParser = new SilhouetteCommandLineParser("LocalSilhouette")
    if(clArgs.parse(args)){
      val logger = Logger.getLogger("it.polito.dbdmg.ontic.Logger")
      
      logger.setLevel(clArgs.args.onticLogLevel)
      Logger.getRootLogger.setLevel(clArgs.args.sparkLogLevel)
      
      //creating spark context
      val conf=new SparkConf().setMaster(clArgs.args.masterUrl)
                    .setAppName("Ontic Spark DBSCAN")
      
      logger.debug("Spark master: " +clArgs.args.masterUrl)
      
      implicit val sc=new SparkContext(conf)
      val startingTime = System.currentTimeMillis()
      
      //reading data
      val parser = Class.forName(clArgs.args.parserClassName).getConstructor().newInstance().asInstanceOf[ClusteredIOParser]
      parser.separator = clArgs.args.parserSeparator
      val startingPoints = sc.textFile(clArgs.args.inputPath).map { parser.parse }.filter(t=>t._1!=0)
      val borderPoints:RDD[(Int, Array[Double])] = if(clArgs.args.borderPath != null) {
        sc.textFile(clArgs.args.borderPath).map {
          parser.parse
        }.filter(t => t._1 != 0)
      } else {
        val centroids = sc.textFile(clArgs.args.modelPath).map { parser.parse }.collect()
        sc.parallelize(BorderSilhouette.takeBorderFromCentroids(startingPoints, centroids, clArgs.args.numCandidates))
      }

      val points = if(clArgs.args.withNoisePoints){
        startingPoints//.union(sc.textFile(clArgs.args.noiseInputPath).map { parser.parse }.filter(t=>t._1==0))
        //we can't compute a border silhouette of noise!!
      }else{
        startingPoints
      }
      val p = clArgs.args.fraction
      val pointsAndSils = BorderSilhouette.compute(points, borderPoints, p)
      IOHandler.saveSilhouetteResult(pointsAndSils, clArgs.args.outputPath)
      
    }
  }

}