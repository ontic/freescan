package it.polito.dbdmg.ontic.util

import org.alitouka.spark.dbscan.spatial.DistanceMeasureSuite
import org.alitouka.spark.dbscan.DbscanSettings
import it.polito.dbdmg.ontic.preprocessing.DBSCANPreprocessingSettings._
import org.apache.log4j.Level


class CommandLineArguments(
  var masterUrl: String = null,
  var inputPath: String = null,
  var outputPath: String = null,
  var distanceMeasureSuite: DistanceMeasureSuite = DbscanSettings.getDefaultDistanceMeasureSuite,
  var hierarchical: Boolean = false,
  var epsilonStep: Double = 1.0,
  var epsilonPolicy:EpsilonPolicies.Value = EpsilonPolicies.BeforeFirstDensityDecrease,
  var minPtsPolicy:MinPointsPolicies.Value = MinPointsPolicies.MyGreedyFunc,
  var normalizationTechnique:NormalizationTechnique.Value = NormalizationTechnique.None,
  var numberOfClusteringLevels:Int = 3,
  var parserClassName:String = "it.polito.dbdmg.ontic.io.TextToPointConverter",
  var parserSeparator:String = ",",
  var sparkLogLevel:Level = Level.WARN,
  var onticLogLevel:Level = Level.INFO,
  var debugOutputPath: Option[String] = None  
  ){
  
}

class CommandLineParser(programName:String, var args:CommandLineArguments=new CommandLineArguments) extends scopt.OptionParser[Unit](programName) {
  opt[String] ("ds-master")
    .foreach { args.masterUrl = _ }
    .required ()
    .valueName ("<url>")
    .text ("Master URL")

  opt[String] ("ds-input")
    .foreach { args.inputPath = _ }
    .required()
    .valueName("<path>")
    .text("Input path")

  opt[String] ("ds-output")
    .foreach { args.outputPath = _ }
    .required()
    .valueName("<path>").text("Output path")

  opt[String] ("distanceMeasureSuite").foreach {
    x => args.distanceMeasureSuite = Class.forName(x).newInstance().asInstanceOf[DistanceMeasureSuite]
  }.valueName("<class>")
  .text("The distance measure suite to be used.")

  opt[Unit] ("hierarchical").foreach {
    x => args.hierarchical = true
  }.text("If set, execute hierarchical clustering")

  opt[String] ("epsStep").foreach {
    x => args.epsilonStep = x.toDouble
  }.valueName("<double>")
  .text("The epsilon step used by the preprocessor.")
  
  opt[String] ("epsPolicy").foreach {
    x => args.epsilonPolicy = EpsilonPolicies.withName(x)
  }.valueName("<enum>")
  .text("The epsilon policy used by the preprocessor.")
  
  opt[String] ("minPointsPolicy").foreach {
    x => args.minPtsPolicy = MinPointsPolicies.withName(x)
  }.valueName("<enum>")
  .text("The minPoints policy used by the preprocessor.")
  
  opt[String] ("normalization").foreach {
    x => args.normalizationTechnique = NormalizationTechnique.withName(x)
  }.valueName("<enum>")
  .text("The normalization technique used by the preprocessor, if any.")
  
  opt[String] ("clusteringLevels").foreach {
    x => args.numberOfClusteringLevels = x.toInt
  }.valueName("<int>")
  .text("The number of times the DBSCAN process is performed in order to get a multilevel result.")
 
  opt[String] ("parserClass").foreach {
    x => args.parserClassName = x
  }.valueName("<class>")
  .text("The class used for parsing the input.")

  opt[String] ("parserSeparator").foreach {
    x => args.parserSeparator = x
  }.valueName("<string>")
  .text("The separator assumed by the parser class.")
  
  opt[String] ("sparkLoggerLevel").foreach {
    x => args.sparkLogLevel = Level.toLevel(x)
  }.valueName("<level>")
  .text("The level used by the spark logger.")
  
  opt[String] ("onticLoggerLevel").foreach {
    x => args.onticLogLevel = Level.toLevel(x)
  }.valueName("<level>")
  .text("The level used by the ontic logger.")
  
  opt[String] ("ds-debugOutput").foreach { x => args.debugOutputPath = Some(x) }

}