package it.polito.dbdmg.ontic.util

import org.alitouka.spark.dbscan.spatial.DistanceMeasureSuite
import org.alitouka.spark.dbscan.DbscanSettings
import it.polito.dbdmg.ontic.preprocessing.DBSCANPreprocessingSettings._
import org.apache.log4j.Level

class ManualDBSCANArguments(
  var masterUrl: String = null,
  var inputPath: String = null,
  var outputPath: String = null,
  var distanceMeasureSuite: DistanceMeasureSuite = DbscanSettings.getDefaultDistanceMeasureSuite,
  var epsilon: Double = 1.0,
  var minPoints : Int = 1,
  var normalizationTechnique:NormalizationTechnique.Value = NormalizationTechnique.None,
  var parserClassName:String = "it.polito.dbdmg.ontic.io.TextToPointConverter",
  var parserSeparator:String = ",",
  var sparkLogLevel:Level = Level.WARN,
  var onticLogLevel:Level = Level.INFO,
  var debugOutputPath: Option[String] = None  
  ){
  
}

class ManualDBSCANCLParser(programName:String, var args:ManualDBSCANArguments=new ManualDBSCANArguments) extends scopt.OptionParser[Unit](programName) {
  opt[String] ("ds-master")
    .foreach { args.masterUrl = _ }
    .required ()
    .valueName ("<url>")
    .text ("Master URL")

  opt[String] ("ds-input")
    .foreach { args.inputPath = _ }
    .required()
    .valueName("<path>")
    .text("Input path")

  opt[String] ("ds-output")
    .foreach { args.outputPath = _ }
    .required()
    .valueName("<path>").text("Output path")

  opt[String] ("distanceMeasureSuite").foreach {
    x => args.distanceMeasureSuite = Class.forName(x).newInstance().asInstanceOf[DistanceMeasureSuite]
  }.valueName("<class>")
  .text("The distance measure suite to be used.")

  opt[String] ("eps").foreach {
    x => args.epsilon = x.toDouble
  }.required().valueName("<double>")
  .text("The epsilon step used by the preprocessor.")
  
  opt[String] ("minPoints").foreach {
    x => args.minPoints = x.toInt
  }.valueName("<enum>").required()
  .text("The minPoints policy used by the preprocessor.")
  
  opt[String] ("normalization").foreach {
    x => args.normalizationTechnique = NormalizationTechnique.withName(x)
  }.valueName("<enum>")
  .text("The normalization technique used by the preprocessor, if any.")
  
  opt[String] ("parserClass").foreach {
    x => args.parserClassName = x
  }.valueName("<class>")
  .text("The class used for parsing the input.")

  opt[String] ("parserSeparator").foreach {
    x => args.parserSeparator = x
  }.valueName("<string>")
  .text("The separator assumed by the parser class.")
  
  opt[String] ("sparkLoggerLevel").foreach {
    x => args.sparkLogLevel = Level.toLevel(x)
  }.valueName("<level>")
  .text("The level used by the spark logger.")
  
  opt[String] ("onticLoggerLevel").foreach {
    x => args.onticLogLevel = Level.toLevel(x)
  }.valueName("<level>")
  .text("The level used by the ontic logger.")
  
  opt[String] ("ds-debugOutput").foreach { x => args.debugOutputPath = Some(x) }

}