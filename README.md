FreeScan
=======

FreeScan is a tool to run DBSCAN without choosing epsilon and minPoints.
It relies on alitouka https://github.com/alitouka/spark_dbscan algorithm to perform DBSCAN clustering over data.

It performs a *multilevel* clustering by estimating automatically epsilon and minPoints.

DiSiLike is a complementary tool to evaluate the goodness of clustering, which exploits a novel technique
 to reduce the complexity of computing the [Silhouette](https://en.wikipedia.org/wiki/Silhouette_(clustering)).
It can be used against clusters of any shape and dimension, thus not only the ones generated by FreeScan.

This library provides also convenience methods to pre-process and post-process the data, like parsing, normalization,
and basic statistics, documented here under, as well as main classes to execute the basic DBSCAN with parameters and Kmeans.

## Input dataset

The tools accept as input row-wise datasets, where each row represents a record.
The default parser splits the row using the chosen separator, e.g. `;`, and use all the features as coordinates for the
clustering.
You can write your own parser following the instructions in [this wiki](https://github.com/alitouka/spark_dbscan/wiki/Data-formats).
Since the tools were developed in a scope of network analitics, we provide already a parser for 
[Tstat](http://tstat.polito.it/) logs and a sample dataset in `sample.tstat`.

## Examples of use

The DBSCAN multilevel computation can be customized and submitted easily.
An example of basic deployment over Spark is:

    spark-submit --class it.polito.dbdmg.ontic.DBSCANDriver ontic_spark_mlsuite-assembly-0.0.3.jar \
    --ds-input somefile.dat --ds-output some_results --ds-master local[*] 


Here we have an example of all the preprocessing options, which is a complete example for the Tstat sample provided:

    spark-submit --class it.polito.dbdmg.ontic.DBSCANDriver ontic_spark_mlsuite-assembly-0.0.3.jar \
    --ds-input sample.tstat --ds-output tstat_results --ds-master local[*] \
    --distanceMeasureSuite EuclideanDistanceSuite \
    --epsStep 0.001 --epsPolicy BeforeMaxDensityDecrease25 --clusteringLevels 4 \
	--parserClass it.polito.dbdmg.ontic.io.TStatConverter --parserSeparator ";" \
    --normalization MinMax --sparkLoggerLevel WARN --onticLoggerLevel INFO


To run Kmeans with the same preprocessing as above:

    spark-submit --class it.polito.dbdmg.ontic.KMeansDriver ontic_spark_mlsuite-assembly-0.0.3.jar \
    --ds-input sample.tstat --ds-output tstat_results --ds-master local[*] \
    --distanceMeasureSuite EuclideanDistanceSuite \
    --K 5 --normalization MinMax \
    --parserClass it.polito.dbdmg.ontic.io.TStatConverter --parserSeparator ";" 



## Command line arguments

In this section all the available command line arguments are described for each driver object available.

### DBSCANDriver
This is the main class for FreeScan.

* `ds-master`: the Spark master URL to be used. This parameter is **required**.
* `ds-input`: the path to the input files. This parameter is **required**.
* `ds-output`: the path in which the result has to be written. This parameter is **required**.
* `distanceMeasureSuite`: the distance measure suite to be used. By default it is `EuclideanDistanceSuite`,
but any class implementing `DistanceMeasureSuite` can be provided, such as `CosineAngleDistanceSuite`
and `ManhattanDistanceSuite`.
* `epsStep`: the value to be set for the _epsStep_ parameter. By default, it is 1.
* `epsPolicy`: the policy to be used for the estimation of _Eps_. By default it is `BeforeFirstDensityDecrease`.
* `minPointsPolicy`: the policy toi be used for the estimation of _MinPts_. By default it is `MyGreedyFunc`.
* `normalization`: the normalization technique to be used. By default it is `None`, which means no 
normalization is performed, but `Zscore` and `MinMax` can be choosen.
* `clusteringLevels`: the number of clustering level to be performed. By default it is 3.
* `parserClass`: the full class name of a class implementing `it.polito.dbdmg.ontic.io.IOParser` which defines how 
to parse a row of the dataset. By default it is `it.polito.dbdmg.ontic.io.TextToPointConverter`.
* `parserSeparator`: the character to be used as separator by the  `parserClass`. By default it is `,`.
* `sparkLoggerLevel`: the logger level the be used for the Spark logger. By default it is `WARN`.
* `onticLoggerLevel`: the logger level the be used for the library logger. By default it is `INFO`.

### ManualDBSCAN
The normal DBSCAN algorithm, with _eps_ and _minPoints_.

* `ds-master`: the Spark master URL to be used. This parameter is **required**.
* `ds-input`: the path to the input files. This parameter is **required**.
* `ds-output`: the path in which the result has to be written. This parameter is **required**.
* `distanceMeasureSuite`: the distance measure suite to be used. By default it is `EuclideanDistanceSuite`,
but any class implementing `DistanceMeasureSuite` can be provided, such as `CosineAngleDistanceSuite`
and `ManhattanDistanceSuite`.
* `eps`: the value to be set for the _eps_ parameter. By default, it is 1.
* `minPoints`: the value to be set for the _minPts_ parameter. By default, it is 1.
* `normalization`: the normalization technique to be used. By default it is `None`, which means no 
normalization is performed, but `Zscore` and `MinMax` can be choosen.
* `parserClass`: the full class name of a class implementing `it.polito.dbdmg.ontic.io.IOParser` which defines how 
to parse a row of the dataset. By default it is `it.polito.dbdmg.ontic.io.TextToPointConverter`.
* `parserSeparator`: the character to be used as separator by the  `parserClass`. By default it is `,`.
* `sparkLoggerLevel`: the logger level the be used for the Spark logger. By default it is `WARN`.
* `onticLoggerLevel`: the logger level the be used for the library logger. By default it is `INFO`.

### KMeansDriver
The KMeans algorithm, as implemented by MLlib.

* `ds-master`: the Spark master URL to be used. This parameter is **required**.
* `ds-input`: the path to the input files. This parameter is **required**.
* `ds-output`: the path in which the result has to be written. This parameter is **required**.
* `K`: the value to be set for the _K_ parameter. By default, it is 5.
* `normalization`: the normalization technique to be used. By default it is `None`, which means no 
normalization is performed, but `Zscore` and `MinMax` can be choosen.
* `parserClass`: the full class name of a class implementing `it.polito.dbdmg.ontic.io.IOParser` which defines how 
to parse a row of the dataset. By default it is `it.polito.dbdmg.ontic.io.TextToPointConverter`.
* `parserSeparator`: the character to be used as separator by the  `parserClass`. By default it is `,`.
* `sparkLoggerLevel`: the logger level the be used for the Spark logger. By default it is `WARN`.
* `onticLoggerLevel`: the logger level the be used for the library logger. By default it is `INFO`.

### BorderSilhouetteDriver

This class implements DiSiLike. If you have run a DBSCAN, you should provide it with a dataset with the border points.
If you have run Kmeans, you should provide the resulting model (the list of the final centroids) plus the number of 
candidate points you want to use for the estimation of the silhouette.

This class assume that the noise points are the points assigned to cluster 0.

* `ds-master`: the Spark master URL to be used. This parameter is **required**.
* `ds-input`: the path to the input files. This parameter is **required**.
* `parserClass`: the full class name of a class implementing `it.polito.dbdmg.ontic.io.ClusteredIOParser` which defines how 
to parse a row of the dataset. By default it is `it.polito.dbdmg.ontic.io.ClusteringResultParser`.
* `parserSeparator`: the character to be used as separator by the  `parserClass`. By default it is `;`.
* `ds-borderInput` : the path of the border points for border Silhouette. The format is the same as for `ds-input`
* `ds-modelInput` : the path of Kmeans model (list of centroids) for border Silhouette. The format is the same as for `ds-input`
* `ds-numCandidates` : the number of points to be elected as border for Kmeans border Silhouette. Default: 100
* `sparkLoggerLevel`: the logger level the be used for the Spark logger. By default it is `WARN`.
* `onticLoggerLevel`: the logger level the be used for the library logger. By default it is `INFO`.



### Stats

It takes two command line arguments: the first is the master URL and the second is the file system location 
of the dataset to be processed.

## How to compile

The project is built with `sbt`, thus compiling it is rather easy. It's enough to go in the root folder of the 
project and then run the command `sbt assembly`. The result of the compilation is a jar which can be
found in `target/scala-2.10`.

